db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "email": "stephenhawking@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "email": "neilarmstrong@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "email": "billgates@mail.com",
        "department": "Operations"
    },

    {
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "email": "janedoe@mail.com",
        "department": "HR"
    }
]);

//FIND USERS LETTER S -FIRST NAME and D- LAST NAME
//$OR OPERATOR

db.users.find({
    $or: [
        {"firstName": {
        $regex: 'S',
        $options: '$i'}
    },
        {"lastName": {
        $regex: 'D',
        $options: '$i'}
    },
    ]
},
    {
        "_id": 0,
        "firstName": 1,
        "lastName": 1
    }

);
//--------------------------------------
//3. Find users who are from the HR department and their age is greater than or equal to 70.


db.users.find({
    $and: [
        {"department": "HR"},
        {"age": {$gte: 50}}
    ]
});

//Find users with the letter e in their first name and has an age of lessthan or equal to 30.a. Use the $and, $regex and $lte operators

db.users.find({
    $and: [
        {"firstName": {
        $regex: 'E',
        $options: '$i'}
    },
    {"age": {$lte: 70}}
    ]
}
);
